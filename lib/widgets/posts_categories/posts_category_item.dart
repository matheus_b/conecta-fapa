class PostsCategoryItem {
  int id;
  String text;
  String assetIcon;
  String networkIcon;

  PostsCategoryItem(this.id, this.text, {this.assetIcon, this.networkIcon});
}
