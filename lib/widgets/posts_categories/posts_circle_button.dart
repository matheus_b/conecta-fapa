import 'package:flutter/material.dart';
import 'package:conectafapa/theme/app_colors.dart';

class PostsCircleButton extends StatelessWidget {
  String label;
  bool selected;
  VoidCallback action;
  String assetIcon;

  PostsCircleButton(this.label, this.selected, this.action, this.assetIcon);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(left: 6, right: 6, bottom: 0, top: 5),
        child: GestureDetector(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                  child: Image.asset(assetIcon,
                      width: 32,
                      height: 32,
                      color: selected
                          ? Colors.white
                          : AppColors.pacificBlue),
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    color: selected
                        ? AppColors.pacificBlue
                        : Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(237, 231, 239, 1),
                        offset: Offset(0, 0),
                        blurRadius: 15,
                      )
                    ],
                  )),
              Container(
                  padding: const EdgeInsets.only(top: 8),
                  width: 80,
                  child: Text(label,
                      softWrap: true,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontWeight: FontWeight.w500,
                          fontSize: 12.7,
                          fontFamily: 'HindMedium',
                          height: 0.8,
                          color: selected
                              ? AppColors.sherpaBlue
                              : Color.fromRGBO(121, 153, 171, 1))))
            ],
          ),
          onTap: action,
        ));
  }
}
