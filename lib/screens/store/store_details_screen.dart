import 'dart:async';

import 'package:conectafapa/models/store.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:conectafapa/theme/app_colors.dart';
import 'package:conectafapa/theme/app_styles.dart';
import 'package:conectafapa/widgets/custom_bottom_navigation_bar.dart';

class StoreDetailsScreen extends StatefulWidget {
  final Store store;

  StoreDetailsScreen(this.store);

  @override
  State createState() => StoreDetailsScreenState();
}

class StoreDetailsScreenState extends State<StoreDetailsScreen> {
  Completer<GoogleMapController> _controller = Completer();
  Map<MarkerId, Marker> marker = <MarkerId, Marker>{};
  CameraPosition _initialPosition;
  String dropdownValue = '';

  @override
  void initState() {
    _initialPosition = CameraPosition(
      target: LatLng(-30.0321453, -51.122529),
      zoom: 16.85,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomBottomNavigationBar(
      SingleChildScrollView(
        child: Stack(
                children: <Widget>[
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.33,
                    child: GoogleMap(
                      gestureRecognizers: Set()
                        ..add(
                          Factory<PanGestureRecognizer>(
                            () => PanGestureRecognizer(),
                          ),
                        )
                        ..add(
                          Factory<ScaleGestureRecognizer>(
                            () => ScaleGestureRecognizer(),
                          ),
                        )
                        ..add(
                          Factory<TapGestureRecognizer>(
                            () => TapGestureRecognizer(),
                          ),
                        )
                        ..add(
                          Factory<VerticalDragGestureRecognizer>(
                            () => VerticalDragGestureRecognizer(),
                          ),
                        ),
                      mapType: MapType.normal,
                      initialCameraPosition: _initialPosition,
                      onMapCreated: (GoogleMapController controller) {
                        _controller.complete(controller);
                      },
                      markers: Set<Marker>.of([Marker(markerId: MarkerId('loja'),
                          position: LatLng(widget.store.lat, widget.store.long),
                          icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed))]),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      GestureDetector(
                        child: Container(
                          margin: EdgeInsets.only(left: 20, top: 20),
                          alignment: Alignment.topLeft,
                          child: Icon(
                            IconData(58820,
                                fontFamily: 'MaterialIcons', matchTextDirection: true),
                            color: Colors.red[800],
                          ),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                        },
                      ),
                      _photo(),
                      Padding(
                        padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            _storeName(),
                            _edifice(),
                            _description(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
      ),
      selected: 'Lojas',
    );
  }

  _photo() {
    return Align(
      alignment: Alignment.centerRight,
      child: Padding(
        padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.15,
          right: MediaQuery.of(context).size.width * 0.05,
        ),
        child: SizedBox(
          height: 100,
          width: 120,
          child: ClipRRect(
            borderRadius: BorderRadius.circular(20),
            child: AspectRatio(
              aspectRatio: 1.1,
              child: Image.asset(
                widget.store.imageURL,
                fit: BoxFit.cover,
              ),
            ),
          ),
        ),
      ),
    );
  }

  _storeName() {
    return Padding(
      padding: EdgeInsets.only(top: 15),
      child: Text(
        widget.store.name,
        style: AppStyles.customStyle(size: 24, fontWeight: FontWeight.bold),
      ),
    );
  }

  String addressFormatter(String logradouro, String numero) {
    if (numero != null && numero != '')
      return "$logradouro, $numero";
    else
      return "$logradouro";
  }

  _edifice() {
    return Padding(
      padding: EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 7),
            child: Text(
              'Local',
              style: AppStyles.defaultTextStyle,
            ),
          ),
          Row(
            children: <Widget>[
              Text(widget.store.edifice,
                  style: AppStyles.customStyle(color: AppColors.astral, height: 1.2)
              ),
            ],
          )
        ],
      ),
    );
  }

  _description() {
    return Padding(
      padding: EdgeInsets.only(top: 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(bottom: 7),
            child: Text(
              'Descrição',
              style: AppStyles.defaultTextStyle,
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Text(widget.store.description,
                    style: AppStyles.customStyle(color: AppColors.astral, height: 1.2)
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
