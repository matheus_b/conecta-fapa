import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:conectafapa/models/post.dart';
import 'package:conectafapa/theme/app_colors.dart';
import 'package:conectafapa/theme/app_styles.dart';
import 'package:conectafapa/widgets/custom_bottom_navigation_bar.dart';

class PostDetailsScreen extends StatefulWidget {
  Post post;

  PostDetailsScreen(this.post);

  @override
  PostDetailsScreenState createState() {
    return new PostDetailsScreenState();
  }
}

class PostDetailsScreenState extends State<PostDetailsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CustomBottomNavigationBar(
      SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[_header(), _post()],
        ),
      ),
      selected: 'Posts',
    );
  }

  _header() {
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.6,
          child: CachedNetworkImage(
              imageUrl: widget.post.imageURL, fit: BoxFit.fill),
        ),
        GestureDetector(
          child: Container(
            padding: EdgeInsets.only(left: 20, top: 20),
            width: 70,
            height: 70,
            alignment: Alignment.topLeft,
            child: Icon(
              IconData(58820,
                  fontFamily: 'MaterialIcons', matchTextDirection: true),
              color: Colors.white,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ],
    );
  }

  _post() {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 30, 20, 30),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _postDateAndAuthor(),
          _postTitle(),
          _postCategory(),
          _postContent()
        ],
      ),
    );
  }

  _postDateAndAuthor() {
    return Padding(
      padding: EdgeInsets.only(bottom: 20),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              widget.post.date,
              style: TextStyle(
                  color: AppColors.pacificBlue,
                  fontWeight: FontWeight.w600,
                  fontFamily: 'HindMedium',
                  height: 0.8),
            ),
          ),
        ],
      ),
    );
  }

  _postTitle() {
    return Text(
      widget.post.title,
      style: AppStyles.customStyle(size: 20, height: 1),
    );
  }

  _postCategory() {
    return Padding(
      padding: EdgeInsets.only(top: 10, bottom: 30),
      child: Text(
        widget.post.category.name,
        style: TextStyle(
            color: AppColors.astral,
            fontWeight: FontWeight.w500,
            fontFamily: 'HindMedium',
            height: 0.8),
      ),
    );
  }

  _postContent() {
    return Html(
      data: """
          ${widget.post.content}
          """,
      defaultTextStyle: TextStyle(
          color: AppColors.astral,
          fontWeight: FontWeight.w300,
          fontSize: 15.2,
          fontFamily: 'HindMedium',
          height: 1.1),
    );
  }
}
